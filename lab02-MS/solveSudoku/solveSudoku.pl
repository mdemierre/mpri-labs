% solve a sudoku with constraint programming model
% the sudoku is given by a problem like the following :
%
%   problem(a(a1,a2,a3,a4,a5,a6,a7,a8,a9),
%           b(b1,b2,b3,b4,b5,b6,b7,b8,b9),
%           c(c1,c2,c3,c4,c5,c6,c7,c8,c9),
%           d(d1,d2,d3,d4,d5,d6,d7,d8,d9),
%           e(e1,e2,e3,e4,e5,e6,e7,e8,e9),
%           f(f1,f2,f3,f4,f5,f6,f7,f8,f9),
%           g(g1,g2,g3,g4,g5,g6,g7,g8,g9),
%           h(h1,h2,h3,h4,h5,h6,h7,h8,h9),
%           i(i1,i2,i3,i4,i5,i6,i7,i8,i9))
%
%           For use this, u need to create a problem.sudoku file
%           who contains the problem in form of a prolog term
%           like following :
%
%           problem(a(1,A2,A3,A4,A5,A6,A7,A8,A9),
%           b(B1,B2,B3,B4,B5,B6,B7,B8,B9),
%           c(C1,C2,C3,C4,C5,C6,C7,C8,C9),
%           d(D1,D2,D3,D4,D5,D6,D7,D8,D9),
%           e(E1,E2,E3,E4,E5,E6,E7,E8,E9),
%           f(F1,F2,F3,F4,F5,F6,F7,F8,F9),
%           g(G1,G2,G3,G4,G5,G6,G7,G8,G9),
%           h(H1,H2,H3,H4,H5,H6,H7,H8,H9),
%           i(7,I2,I3,I4,I5,I6,I7,I8,I9)).
%
%           This is a problem with 81 free square
%           u can replace undefined variable with digit atom

% first agument call
:-initialization(main).

% main program
% exit gprologafter his call
main :-
    open('problem.sudoku',read,Stream),
    read_term(Stream,Problem,[]),
    write(Problem),nl,
    solve(Problem),
    close(Stream),halt.
main :- write('No solution found'),nl,halt.

solve(problem(a(A1,A2,A3,A4,A5,A6,A7,A8,A9),
b(B1,B2,B3,B4,B5,B6,B7,B8,B9),
c(C1,C2,C3,C4,C5,C6,C7,C8,C9),
d(D1,D2,D3,D4,D5,D6,D7,D8,D9),
e(E1,E2,E3,E4,E5,E6,E7,E8,E9),
f(F1,F2,F3,F4,F5,F6,F7,F8,F9),
g(G1,G2,G3,G4,G5,G6,G7,G8,G9),
h(H1,H2,H3,H4,H5,H6,H7,H8,H9),
i(I1,I2,I3,I4,I5,I6,I7,I8,I9))):-
    List = [A1,A2,A3,A4,A5,A6,A7,A8,A9,
    B1,B2,B3,B4,B5,B6,B7,B8,B9,
    C1,C2,C3,C4,C5,C6,C7,C8,C9,
    D1,D2,D3,D4,D5,D6,D7,D8,D9,
    E1,E2,E3,E4,E5,E6,E7,E8,E9,
    F1,F2,F3,F4,F5,F6,F7,F8,F9,
    G1,G2,G3,G4,G5,G6,G7,G8,G9,
    H1,H2,H3,H4,H5,H6,H7,H8,H9,
    I1,I2,I3,I4,I5,I6,I7,I8,I9],
    fd_domain(List,1,9),            %each number are only between 1 and 9
    % rows are all different
    fd_all_different([A1,A2,A3,A4,A5,A6,A7,A8,A9]),
    fd_all_different([B1,B2,B3,B4,B5,B6,B7,B8,B9]),
    fd_all_different([C1,C2,C3,C4,C5,C6,C7,C8,C9]),
    fd_all_different([D1,D2,D3,D4,D5,D6,D7,D8,D9]),
    fd_all_different([E1,E2,E3,E4,E5,E6,E7,E8,E9]),
    fd_all_different([F1,F2,F3,F4,F5,F6,F7,F8,F9]),
    fd_all_different([G1,G2,G3,G4,G5,G6,G7,G8,G9]),
    fd_all_different([H1,H2,H3,H4,H5,H6,H7,H8,H9]),
    fd_all_different([I1,I2,I3,I4,I5,I6,I7,I8,I9]),
    % cols are all different
    fd_all_different([A1,B1,C1,D1,E1,F1,G1,H1,I1]),
    fd_all_different([A2,B2,C2,D2,E2,F2,G2,H2,I2]),
    fd_all_different([A3,B3,C3,D3,E3,F3,G3,H3,I3]),
    fd_all_different([A4,B4,C4,D4,E4,F4,G4,H4,I4]),
    fd_all_different([A5,B5,C5,D5,E5,F5,G5,H5,I5]),
    fd_all_different([A6,B6,C6,D6,E6,F6,G6,H6,I6]),
    fd_all_different([A7,B7,C7,D7,E7,F7,G7,H7,I7]),
    fd_all_different([A8,B8,C8,D8,E8,F8,G8,H8,I8]),
    fd_all_different([A9,B9,C9,D9,E9,F9,G9,H9,I9]),
    % square A B C
    fd_all_different([A1,A2,A3,B1,B2,B3,C1,C2,C3]),
    fd_all_different([A4,A5,A6,B4,B5,B6,C4,C5,C6]),
    fd_all_different([A7,A8,A9,B7,B8,B9,C7,C8,C9]),
    % square D E F
    fd_all_different([D1,D2,D3,E1,E2,E3,F1,F2,F3]),
    fd_all_different([D4,D5,D6,E4,E5,E6,F4,F5,F6]),
    fd_all_different([D7,D8,D9,E7,E8,E9,F7,F8,F9]),
    % square G H I
    fd_all_different([G1,G2,G3,H1,H2,H3,I1,I2,I3]),
    fd_all_different([G4,G5,G6,H4,H5,H6,I4,I5,I6]),
    fd_all_different([G7,G8,G9,H7,H8,H9,I7,I8,I9]),
    fd_labelingff(List), % search solution
    % print solution to the console
    format('+---+---+---+',[]),nl,
    format('|%d%d%d|%d%d%d|%d%d%d|',[A1,A2,A3,A4,A5,A6,A7,A8,A9]),nl,
    format('|%d%d%d|%d%d%d|%d%d%d|',[B1,B2,B3,B4,B5,B6,B7,B8,B9]),nl,
    format('|%d%d%d|%d%d%d|%d%d%d|',[C1,C2,C3,C4,C5,C6,C7,C8,C9]),nl,
    format('+---+---+---+',[]),nl,
    format('|%d%d%d|%d%d%d|%d%d%d|',[D1,D2,D3,D4,D5,D6,D7,D8,D9]),nl,
    format('|%d%d%d|%d%d%d|%d%d%d|',[E1,E2,E3,E4,E5,E6,E7,E8,E9]),nl,
    format('|%d%d%d|%d%d%d|%d%d%d|',[F1,F2,F3,F4,F5,F6,F7,F8,F9]),nl,
    format('+---+---+---+',[]),nl,
    format('|%d%d%d|%d%d%d|%d%d%d|',[G1,G2,G3,G4,G5,G6,G7,G8,G9]),nl,
    format('|%d%d%d|%d%d%d|%d%d%d|',[H1,H2,H3,H4,H5,H6,H7,H8,H9]),nl,
    format('|%d%d%d|%d%d%d|%d%d%d|',[I1,I2,I3,I4,I5,I6,I7,I8,I9]),nl,
    format('+---+---+---+',[]),nl,
    format('',[]),nl,!. % cut, we dont want more solution, only one is enough

