from sklearn.metrics import confusion_matrix, classification_report
import pylab as pl


def show_confusion_matrix(y_true, y_predicted, title='Confusion matrix'):
    """
    Plot (and print) a confusion matrix from y_true and y_predicted
    """

    # Compute and print confusion matrix
    cm = confusion_matrix(y_true, y_predicted)
    print(title)
    print("-" * len(title))
    print(cm)

    # Show confusion matrix in a separate window
    pl.matshow(cm)
    pl.title(title)
    pl.colorbar()
    pl.ylabel('True label')
    pl.xlabel('Predicted label')
    pl.show()


def print_classification_report(y_true, y_pred, title='Classification report'):
    """
    Print a classification report
    """
    print(title)
    print("-" * len(title))
    print(classification_report(y_true, y_pred))
