# coding=utf-8
import os
import pickle
import numpy as np
from sklearn import svm, cross_validation
from classification.metrics import show_confusion_matrix, print_classification_report
from image.feature_extraction import load_data
from sklearn.grid_search import GridSearchCV


def train(clf, X_train, y_train):
    """ Train and return an SVM classifier """

    # train the classifier
    clf.fit(X_train, y_train)

    # return the classifier
    return clf


def load_or_train(force_train=False, grid_search=False):
    """
    Load an existing one or train a new SVM classifier, and return it.
    Once the classifier is trained, it is saved through pickle.
    """

    clf_path='./clf.pkl'
    data_path = "././data/ocr_data/"

    clf = None

    if not force_train and os.path.exists(clf_path):
        clf = pickle.load(open(clf_path, 'rb'))
    else:
        # Loading all data
        X, y = load_data(data_path)

        if grid_search :
            # Instantiate and train a classifier using grid search
            C_range = np.logspace(-3, 3, 7)  # 0.001 - 1000
            gamma_range = np.logspace(-3, 2, 6)  # 0.001 - 100
            param_grid = [
                {'kernel': ['linear'], 'C': C_range},
                {'kernel': ['rbf'], 'gamma': gamma_range, 'C': C_range}
            ]
            kfolds = 5  # number of folds
            n_jobs = 7  # use multicore to compute in parallel
            clf = GridSearchCV(estimator=svm.SVC(), param_grid=param_grid, cv=kfolds, n_jobs=n_jobs, refit=True)
            clf.fit(X, y)

            # Print optimal parameters
            print("Best parameters set found on training set:")
            print
            print(clf.best_params_)
            print

            # print scores for all parameters
            print("Grid scores on training set:")
            print
            for params, mean_score, scores in clf.grid_scores_:
                print("%0.3f (+/-%0.03f) for %r"
                    % (mean_score, scores.std() * 2, params))
        else:
            clf = svm.SVC(kernel="linear", C=10)
            #clf = svm.SVC(kernel="rbf", C=10, gamma=0.01)

            # Train the classifier on the whole dataset
            clf = train(clf, X, y)

        # Save the classifier
        pickle.dump(clf, open(clf_path, 'wb'))

        # If you want, you can do validation, print classification report and show confusion matrix with this
        # trained classifier. But keep in mind that you will do it on the training set itself!
        y_predicted = clf.predict(X)
        print_classification_report(y, y_predicted, "Training classification report")
        show_confusion_matrix(y, y_predicted, "Training confusion matrix")

    # return the classifier
    return clf
