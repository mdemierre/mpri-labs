import os

import numpy as np
from skimage.io import imread

from classification.metrics import show_confusion_matrix, print_classification_report
from classification.svm import load_or_train
from image.cell_extraction import extract_cells
from image.feature_extraction import extract_features

# Choose a sudoku grid number, and prepare paths (image and verification grid)
sudoku_nb = 3
im_path = './data/sudokus/sudoku{}.JPG'.format(sudoku_nb)
ver_path = './data/sudokus/sudoku{}.sud'.format(sudoku_nb)

# Get trained classifier
print("Training/Loading classifier...")
classifier = load_or_train(force_train=False, grid_search=False)

# Load sudoku image as a gray level images
image = imread(im_path, as_grey=True)

# Extract cells
print("Extracting cells... (user input needed)")
extracted_cells = extract_cells(image, im_path)

# Extract features for each cell
print("Extracting features...")
features = []
for cell in extracted_cells:
    features.append(extract_features(cell))

# Classification
print("Predicting...")
y_out = classifier.predict(features)

print(y_out.reshape(81))

# Load solution to compare with, print metrics, and print confusion matrix
y_sudoku = np.loadtxt(ver_path).reshape(81)
print_classification_report(y_sudoku, y_out)
show_confusion_matrix(y_sudoku, y_out)

# Print resulting sudoku grid
print
title = "Resolving sudoku grid"
print(title)
print("-" * len(title))
print(y_out.reshape((9, 9)))

# Solve Sudoku
problem_file = open('problem.sudoku', 'w')
fmt = "problem(a(%s,%s,%s,%s,%s,%s,%s,%s,%s)," \
      "b(%s,%s,%s,%s,%s,%s,%s,%s,%s)," \
      "c(%s,%s,%s,%s,%s,%s,%s,%s,%s)," \
      "d(%s,%s,%s,%s,%s,%s,%s,%s,%s)," \
      "e(%s,%s,%s,%s,%s,%s,%s,%s,%s)," \
      "f(%s,%s,%s,%s,%s,%s,%s,%s,%s)," \
      "g(%s,%s,%s,%s,%s,%s,%s,%s,%s)," \
      "h(%s,%s,%s,%s,%s,%s,%s,%s,%s)," \
      "i(%s,%s,%s,%s,%s,%s,%s,%s,%s))."
problem_file.write((fmt % tuple(y_out)).replace("0", "_"))
problem_file.close()
os.system("./solveSudoku")
print("")
print("Finished!")
