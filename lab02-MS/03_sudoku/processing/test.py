import matplotlib
import matplotlib.pyplot as plt

from  skimage.filters import threshold_otsu, threshold_adaptive
from  skimage.io import imread

matplotlib.rcParams['font.size'] = 9

image = imread("../data/ocr_data/1_99.jpg")
thresh = threshold_otsu(image)
binary = image > thresh

block_size = 20
binary_adapt = threshold_adaptive(image, block_size=block_size)

fig, (ax1, ax2, ax3, ax4) = plt.subplots(1,4, figsize=(8, 2.5))
ax1.imshow(image, cmap=plt.cm.gray)
ax1.set_title('Original')
ax1.axis('off')

ax2.hist(image)
ax2.set_title('Histogram')
ax2.axvline(thresh, color='r')

ax3.imshow(binary, cmap=plt.cm.gray)
ax3.set_title('Thresholded')
ax3.axis('off')

ax4.imshow(binary_adapt, cmap=plt.cm.gray)
ax4.set_title('Thresholded (adaptative)')
ax4.axis('off')



plt.show()
