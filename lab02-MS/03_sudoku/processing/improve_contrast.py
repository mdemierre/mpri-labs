import matplotlib
import numpy as np
import matplotlib.pyplot as plt

from skimage import exposure
from skimage import filters
from skimage.io import imread
from skimage.transform import resize
from skimage import util

__author__ = 'marc'

matplotlib.rcParams['font.size'] = 9
im_size = 50
border_size = 5

fig, (ax1, ax2, ax3, ax4, ax5) = plt.subplots(1, 5)

image = imread("../data/ocr_data/8_45.jpg", as_grey=True)
image = resize(image, (im_size, im_size), preserve_range=True)
ax1.imshow(image, cmap=plt.cm.gray)
ax1.set_title('Original')
ax1.axis('off')


#im = image[border_size:-border_size, border_size:-border_size]
im = util.crop(image, border_size)

ax2.imshow(im, cmap=plt.cm.gray)
ax2.set_title('Original (cropped)')
ax2.axis('off')


p2, p98 = np.percentile(im, (5, 60))
im_rescaled = exposure.rescale_intensity(im, in_range=(p2, p98))

ax3.imshow(im_rescaled, cmap=plt.cm.gray)
ax3.set_title('Rescaled intensity')
ax3.axis('off')

thresh = filters.threshold_otsu(im)
im_binarized = im > thresh


ax4.imshow(im_binarized, cmap=plt.cm.gray)
ax4.set_title('Binarized')
ax4.axis('off')


thresh = filters.threshold_otsu(im_rescaled)
im_rescaled_binarized = im_rescaled > thresh

ax5.imshow(im_rescaled_binarized, cmap=plt.cm.gray)
ax5.set_title('Rescaled + Binarized')
ax5.axis('off')

# Equalization
img_eq = exposure.equalize_hist(im)

plt.show()
