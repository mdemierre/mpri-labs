from matplotlib import pyplot as plt

from skimage.transform import resize
from skimage.io import imread
from  skimage.filters import threshold_otsu


def process_img(image):
    thresh = threshold_otsu(image)
    process_image = image > thresh

    for i in range(0, 64, 4):
        for j in range(0, 64, 4):
            if process_image[i:i + 4, j:j + 4].sum() <= 6:
                process_image[i:i + 4, j:j + 4] = 0
            else:
                process_image[i:i + 4, j:j + 4] = 1

    return process_image


def do_process(img_path):
    image = imread(img_path)

    im_size = 64
    border_size = 4
    image = image[border_size:-border_size, border_size:-border_size]
    image = resize(image, (im_size, im_size))

    p_image = process_img(image)

    return image, p_image

if __name__ == '__main__':
    for i in range(10):
        im, p_im = do_process("../data/ocr_data/"+str(i)+"_25.jpg")

        fig, (ax1, ax2) = plt.subplots(1, 2)
        ax1.imshow(im, interpolation='nearest', cmap=plt.cm.gray)
        ax1.axis((0, 64, 64, 0))
        ax2.imshow(p_im, interpolation='nearest', cmap=plt.cm.gray)
        ax2.axis((0, 64, 64, 0))
        plt.show()
