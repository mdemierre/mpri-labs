import matplotlib
import numpy as np
import matplotlib.pyplot as plt

from skimage.feature import hog
from skimage.transform import resize
from skimage.util import crop
from skimage.exposure import rescale_intensity
from PIL import Image

__author__ = 'marc'


def rescale(im) :
    return rescale_intensity(im, in_range=(0, 0.02))

matplotlib.rcParams['font.size'] = 9
im_size = 50
border_size = 5

orientations = 9
cells_per_block = (2, 2)

fig, ax = plt.subplots(10, 6)

for num in range(0, 10):
    image = np.array(Image.open("../data/ocr_data/{}_15.jpg".format(num)).convert('L'))
    image = resize(image, (im_size, im_size))
    im = crop(image, border_size)

    hog_4, vis_4 = hog(im, orientations=orientations, pixels_per_cell=(4, 4), cells_per_block=cells_per_block, visualise=True, normalise=True)
    hog_6, vis_6 = hog(im, orientations=orientations, pixels_per_cell=(6, 6), cells_per_block=cells_per_block, visualise=True, normalise=True)
    hog_8, vis_8 = hog(im, orientations=orientations, pixels_per_cell=(8, 8), cells_per_block=cells_per_block, visualise=True, normalise=True)
    hog_10, vis_10 = hog(im, orientations=orientations, pixels_per_cell=(10, 10), cells_per_block=cells_per_block, visualise=True, normalise=True)
    hog_12, vis_12 = hog(im, orientations=orientations, pixels_per_cell=(12, 12), cells_per_block=cells_per_block, visualise=True, normalise=True)

    ax[num][0].imshow(im, cmap=plt.cm.gray)
    ax[num][0].set_title('Image')
    ax[num][0].axis('off')

    ax[num][1].imshow(rescale(vis_4), cmap=plt.cm.gray)
    ax[num][1].set_title('HOG 4')
    ax[num][1].axis('off')

    ax[num][2].imshow(rescale(vis_6), cmap=plt.cm.gray)
    ax[num][2].set_title('HOG 6')
    ax[num][2].axis('off')

    ax[num][3].imshow(rescale(vis_8), cmap=plt.cm.gray)
    ax[num][3].set_title('HOG 8')
    ax[num][3].axis('off')

    ax[num][4].imshow(rescale(vis_10), cmap=plt.cm.gray)
    ax[num][4].set_title('HOG 10')
    ax[num][4].axis('off')

    ax[num][5].imshow(rescale(vis_12), cmap=plt.cm.gray)
    ax[num][5].set_title('HOG 12')
    ax[num][5].axis('off')

plt.show()

