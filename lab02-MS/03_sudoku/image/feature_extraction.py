from skimage.filters import threshold_otsu
from skimage.transform import resize
from skimage.io import imread
from skimage.feature import hog
from skimage.util import crop
from skimage.filters import threshold_otsu

from tools import list_images

BORDER_SIZE = 5
IM_SIZE = 50
STEP = 5
THRESHOLD = 10


def extract_features(im):
    """ Returns a feature vector for an image patch. """
    # TODO: find other features to use
    hog_6 = hog(im, orientations=9, pixels_per_cell=(6, 6), cells_per_block=(2, 2), normalise=True)
    return hog_6


def preprocess_image(im):
    """
    Pre-processing of a cell to improve image
    :param im:
    :return:
    """

    # Binarization with otsu
    # thresh = threshold_otsu(im)
    # im = im > thresh

    # Rescale contrast
    # p2, p98 = np.percentile(im, (10, 60))
    # im_rescaled = exposure.rescale_intensity(im, in_range=(p2, p98))

    # for i in range(0, IM_SIZE, STEP):
    #     for j in range(0, IM_SIZE, STEP):
    #         if im[i:i + STEP, j:j + STEP].sum() <= THRESHOLD:
    #             im[i:i + STEP, j:j + STEP] = 0
    #         else:
    #             im[i:i + STEP, j:j + STEP] = 1

    return im


def process_image(im, border_size=BORDER_SIZE, im_size=IM_SIZE):
    """ Remove borders and resize """

    im = resize(im, (im_size, im_size), preserve_range=True)
    im = crop(im, border_size)
    im = preprocess_image(im)
    return im


def load_data(path):
    """ Return labels and features for all jpg images in path. """

    # Create a list of all files ending in .jpg
    im_list = list_images(path, '.jpg')

    # Create labels
    labels = [int(im_name.split('/')[-1][0]) for im_name in im_list]

    features = []
    for image in im_list:
        crt_image = imread(image, as_grey=True)
        crt_image = process_image(crt_image)
        feature = extract_features(crt_image)
        features.append(feature)

    return features, labels
