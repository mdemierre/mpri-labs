% ==============================
% Authors of the LaTeX template:
%   - Sylvain Julmy
%   - Marc Demierre
% ==============================

\documentclass[a4paper,11pt]{report}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage[english]{babel}
\usepackage{graphicx}
\usepackage{fullpage}
\usepackage{fancyhdr}
\usepackage{color}
\usepackage{blindtext}
\usepackage{titlesec}
\usepackage{minted}
\usepackage{lscape}
\usepackage{tikz}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{listings}
\lstset{language=Matlab, frame=single, tabsize=2, basicstyle=\footnotesize}
%supprime le retrait de paragraphe
\parindent=0em

\newcommand{\Ecole}{University of Applied Sciences Western Switzerland}
\newcommand{\Filiere}{MSE - Software Engineering}
\newcommand{\Cours}{MPRI}
\newcommand{\Titre}{S01 \\ Biometric Applications and Artificial Neural Networks}
\newcommand{\Lieu}{Lausanne}
\newcommand{\ReferentA}{M. Jean Hennebert}
\newcommand{\PartA}{Marc Demierre}
\newcommand{\PartB}{Sylvain Julmy}
\newcommand{\PartC}{Bryan Piller}
\newcommand{\PartD}{Maria Sisto}

\newcommand{\Parts}{\PartA \\ \PartB \\ \PartC \\ \PartD}
\newcommand{\Referents}{\ReferentA}

\pagestyle{fancy}
\lhead[]{\Cours}
\chead[]{}
\rhead[]{\Lieu, \today}

\setlength{\headheight}{14pt}
\setlength{\headsep}{14pt}

\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}

%supprime les mot "Chapitre N" dans le document
\titleformat{\chapter}{}{\bf\LARGE\thechapter. \space}{0em}{\bf\LARGE}

% Code styles for highlighting
\newminted{java}{frame=single, framesep=6pt, breaklines=true}
\newminted{matlab}{frame=single, framesep=6pt, breaklines=true}
\newminted{text}{frame=single, framesep=6pt, breaklines=true}

\begin{document}

\begin{titlepage}
    \begin{center}

        % only works if a paragraph has started.
        \includegraphics[width=0.8\textwidth]{img/mse_logo}~\\[1.5cm]
        \textsc{\Large \Ecole}\\[0.25cm]
        \textsc{\Large \Filiere}\\[1.5cm]
        \textsc{\LARGE \Cours}\\[0.5cm]

        % Title
        \HRule \\[0.4cm]
        { \huge \bfseries \Titre\\[0.4cm] }
        \HRule \\[1.5cm]

        % Author and supervisor
        \begin{minipage}[t]{0.4\textwidth}
            \begin{flushleft} \Large
                \emph{Auteurs:}\\ \Parts
            \end{flushleft}
        \end{minipage}
        \begin{minipage}[t]{0.4\textwidth}
            \begin{flushright} \Large
                \emph{Superviseurs:}\\\Referents
            \end{flushright}
        \end{minipage}~\\[1.5cm]

        \begin{center}
            \includegraphics[scale=0.7]{img/logo_hes-so}
        \end{center}

        \vfill

        % Bottom of the page
        {\large \Lieu, \today}

    \end{center}
\end{titlepage}

\section*{Biometric System Evaluation}
\subsection*{Genuine and imposture distribution}
Here are the histograms for the genuine and imposture data:

\includegraphics[width=\textwidth]{img/hist.png}

We used the \textbf{hist} MATLAB function to plot the histogram of both sets.
\begin{matlabcode}
genuine_scores = load('TP01-1-genuine.txt');  
impostures_scores = load('TP01-1-impostures.txt');

figure;
subplot(1,2,1);hist(genuine_scores);title('Genuine scores');
subplot(1,2,2);hist(impostures_scores);title('Imposture scores');
\end{matlabcode}

\subsection*{Biometric performance curves}
We need to compute FA and FR in regards to the possible values of T.
\begin{matlabcode}
% -- Compute false acceptance and reject
step = 0.001;
min_score = min([min(genuine_scores), min(impostures_scores)]);
max_score = max([max(genuine_scores), max(impostures_scores)]);
T = min_score:step:max_score;
FR = zeros(1,size(T,2));
FA = zeros(1,size(T,2));

for tt = 1:size(T,2)
  Ttmp = T(tt);
  FR(tt) = sum(genuine_scores < Ttmp) ./ size(genuine_scores, 1);
  FA(tt) = sum(impostures_scores >= Ttmp) ./ size(impostures_scores, 1);
end

% -- T-FA/T-FR curves
figure;
plot(T,FA,'r'); hold on;
plot(T,FR,'b'); 

xlabel('Threshold');
ylabel('False Acceptance/Reject probability (%)');
legend('FA', 'FR')
title('T-FA, T-FR');
\end{matlabcode}

We plot FA and FR according to the possible values of T (Threshold).\\
\includegraphics[width=0.8\textwidth]{img/FAFR.png}

We plot FA and FR against each other in the ROC curve.\\
\includegraphics[width=0.8\textwidth]{img/ROC.png}

Since the ROC curve is not very readable, we replot it with a logarithmic scale to get a standard DET curve.\\
\includegraphics[width=0.8\textwidth]{img/DET.png}

\subsection*{Equal Error Rate}
The error rate can be found by intersecting the DET curve with the diagonal. This intersection is the point where the FA and FR are equal.\\
\includegraphics[width=0.8\textwidth]{img/EER.png}

The EER is about 6.402.

\subsection*{Optimal value of Threshold}
Optimal cost: 0.0927, Optimal threshold: -0.7616 \\
\includegraphics[width=0.8\textwidth]{img/CDET.png}

\begin{matlabcode}
% -- CDET curve

% Settings
cost_fr        = 2;
cost_fa        = 1;
prob_genuine   = 0.5;
prob_imposture = 0.5;

% Compute costs
CDET = FR .* cost_fr .* prob_genuine + FA .* cost_fa .* prob_imposture;

% Find min threshold and its cost
[optimal_cost, min_idx] = min(CDET);
optimal_threshold = T(min_idx);

% Display
figure;
hold on;
plot(T, CDET, 'r'); 
plot(optimal_threshold, optimal_cost, 'bo');
title('CDET');
xlabel('Threshold');
ylabel('Cost');
\end{matlabcode}

\newpage
\section*{ANN to estimate a cosine function}

\subsection*{Generate a training set of couples $\{(x, f (x))\}$}

\begin{matlabcode}

% Part (a)
% Generate a sample of N random data between [-20;20]
% and save him into a file

N = 1000;
dataX = rand(1,N/2).*20;
dataX = [dataX,rand(1,N/2).*-20];
dataX = dataX(randperm(N));
dataY = feval(fn,dataX);
data = [dataX;dataY];
save('data.mat','data');
\end{matlabcode}

\subsection*{Train a 3-layers MLP using a varying number of neurons}
We used a MATLAB tool called \texttt{feedforwadnet} and we tried the training with these values \texttt{nHidden = \{5,10,25,50,100\}}.

The code :
\begin{matlabcode}
% number of neurons in the hidden layer
nHidden = 25;

% net.layers{1}.transferFcn : tansig
net = feedforwardnet(nHidden);

%net.trainParam.showWindow = false;
% net.trainParam.epochs = 20;

[net,tr] = train(net,dataX,dataY); 
\end{matlabcode}

This tool uses the \texttt{tansig} tranfer function in the hidden layer, which outputs a value in the range $[-1,1]$. The output layer uses the \texttt{purelin} function, which also has an output in the $[-1,1]$ range but is linear.

\subsection*{Compute the value of the ouput for a range of x value. Plot it}
With 1000 points and 25 hidden neurons, we got very good results:
\begin{center}
\includegraphics[scale=0.32]{img/ex2_plotfit_25.png}
\includegraphics[scale=0.405]{img/ex2_plotperform_25.png}
\end{center} 

We can also see different results with less neurons, for example 10 neurons:
\begin{center}
\includegraphics[scale=0.32]{img/ex2_plotfit_10.png}
\includegraphics[scale=0.405]{img/ex2_plotperform_10.png}
\end{center} 

And 5 neurons :
\begin{center}
\includegraphics[scale=0.32]{img/ex2_plotfit_5.png}
\includegraphics[scale=0.405]{img/ex2_plotperform_5.png}
\end{center} 

With less neurons we can observe a bigger error.

Finally, we tried the ANN with 50 and 100 hidden neurons and observed that precision wasn't better. So the optimal number of hidden neurons is between 20 and 25.

50 neurons:

\begin{center}
\includegraphics[scale=0.32]{img/ex2_plotfit_50.png}
\includegraphics[scale=0.405]{img/ex2_plotperform_50.png}
\end{center} 

100 neurons:

\begin{center}
\includegraphics[scale=0.32]{img/ex2_plotfit_100.png}
\includegraphics[scale=0.405]{img/ex2_plotperform_100.png}
\end{center} 

\newpage

\section*{Train your biometric system}

\subsection*{Extract the features of all audio files}

\texttt{extractWav.m} creates a directory named \texttt{wavFiles} and calls the function to extract data:
\begin{matlabcode}
wav_directory = 'wavFiles/';

files = dir(wav_directory);
for file = files'
    if file.isdir
        continue;
    end
    FeatureGeneratorTxt(file.name, wav_directory);
end
\end{matlabcode}

We just changed the given code \texttt{FeaturesGeneratorTxt.m} to put the extracted feature files in a separate folder:
\begin{matlabcode}
function FeatureGeneratorTxt(file,path)
    addpath('feature-extraction'); 
    if strcmp(file,'.')
        return;
    end
    if strcmp(file,'..')
        return;
    end
    fprintf('%s\n',[path file]);
    [training_data,Fs,bits] = wavread([path file]);
    c = melcepst(training_data,Fs);

    csvwrite(['txtFiles/' file '.txt'], c);
end
\end{matlabcode}

\texttt{createDataFile.m} create one big file with all features and class labels. With this file we can load all data at once in a matrix.

\begin{matlabcode}
function [] = createDataFile(srcDir,dataFile)
%CREATEDATAFILE Loads the different text files, and saves the features and class labels in a single text file.

if exist(dataFile) %#ok<EXIST>
    return;
end

% setup files info
files = dir(srcDir);
classes = [];
% Create the data file to simplify coding
% and not use semi-dinamyc array of matlab (too slow)
fileID = fopen(dataFile,'w');
numclasse = 0;
lastName = ''; % just init
for file = files'
    % recover some data
    if file.isdir 
        % filter '.', '..', ...
        continue;
    end
    
    % Get the classe name
    classeName = strsplit(file.name,'-'); classeName = classeName(1);
    classes = [classes;classeName]; %#ok<AGROW>
    
    % determine classes
    if ~strcmp(classeName,lastName)
        numclasse = numclasse + 1;
    end
    lastName = classeName;
    
    % load file and save it (append) to the full data file
    name = [srcDir '/' file.name];
    tmpFile = load(name);
    for ii = 1:size(tmpFile,1)
        fprintf(fileID,'%s %d\n',sprintf('%d,',tmpFile(ii,:)),numclasse);
    end
end
fclose(fileID);
end
\end{matlabcode}

\textbf{Warning}: feedforwardnet expects an input value in a matrix where each line represents a features and each column represent a sample. So we need to transpose the matrix like this:

\begin{matlabcode}
data = data'; % need to transpose for network use
\end{matlabcode} 

\subsection*{Train a 2-class-output MLP with 3-layers using the training set data}

The code is similar to exercise 2. We use \texttt{feedforwardnet} to create a neural network with 10 hiddens neurons, and we extract features and class (=groundTruth) to train the network. After that, we retrieve the validation set and compute scores with the function \texttt{sim}.

The code :
\begin{matlabcode}

% classId : person to recognize e.g 1 = Marc Demierre
% data    : all data extract from wav file

function [net,tr,scores,classVal] = trainNN(data,classId)

% Create a new NN for a specific ID class

net = feedforwardnet(10);
net.trainParam.showWindow = true;

% shuffle data
data = data(:,randperm(size(data,2)));

% x data : all line except the last (features)
% y data : classe of the features
class = data(end,:);
features = data(1:end-1,:);
class = class(end,:) == classId;

[net,tr] = train(net,features,class);

% validation of the result
featVal = features(:,tr.valInd);
classVal = class(:,tr.valInd);
scores = sim(net,featVal);

end
\end{matlabcode}

Example of result and the performance graph (plotperform):

\begin{matlabcode}
scores = 0.0445   -0.0346   -0.2158    0.8824    0.3814    0.0058
\end{matlabcode}

\begin{center}
\includegraphics[scale=0.5]{img/ex3_plotperform.png}
\end{center}



\subsection*{From the scores, plot a DET curve}
We used the same code as exercise 1 to plot DET curve.

The data comes from the results of the validation set only, to estimate performances for "real" data and get no influence from the training set.

Our result:

\begin{center}
\includegraphics[scale=0.5]{img/ex3_det.png}
\end{center}

The EER is at around 6.84.

We also plotted the FA/FR curve and ROC curve:

\begin{center}
\includegraphics[scale=0.4]{img/ex3_fafr.png}
\includegraphics[scale=0.4]{img/ex3_roc.png}
\end{center}



\end{document}

