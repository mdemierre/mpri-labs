% TP01 - ex01
% ===========

% Authors : Sylvain Julmy, Marc Demierre, Bryan Piller

% % -- Initialization
close all;
clear all;

genuine_scores = load('TP01-1-genuine.txt');  
impostures_scores = load('TP01-1-impostures.txt');

% -- Scores distribution histograms
figure;
subplot(1,2,1);hist(genuine_scores);title('Genuine scores');
subplot(1,2,2);hist(impostures_scores);title('Imposture scores');


% -- Compute false acceptance and reject
step = 0.001;
min_score = min([min(genuine_scores), min(impostures_scores)]);
max_score = max([max(genuine_scores), max(impostures_scores)]);
T = min_score:step:max_score;
FR = zeros(1,size(T,2));
FA = zeros(1,size(T,2));

for tt = 1:size(T,2)
    Ttmp = T(tt);
    FR(tt) = sum(genuine_scores < Ttmp) ./ size(genuine_scores, 1);
    FA(tt) = sum(impostures_scores >= Ttmp) ./ size(impostures_scores, 1);
end


% -- T-FA/T-FR curves
figure;
plot(T,FA,'r'); hold on;
plot(T,FR,'b'); 

xlabel('Threshold');
ylabel('False Acceptance/Reject probability (%)');
legend('FA', 'FR')
title('T-FA, T-FR');


% -- ROC curve
figure;
plot(FA .* 100, FR .* 100, 'b');
xlabel('False Acceptance probability (%)');
ylabel('False Reject probability (%)');
title('ROC');


% -- DET curve
figure;

% DET curve
loglog(FA.*100,FR.*100,'g'); 
axis([0 100 0 100]);hold on;
% diagonal
x = 0:0.01:100;
plot(x,x,'r');

% Plot settings
title('DET');
axis([0 100 0 100]);
xlabel('False Acceptance probability (%)');
ylabel('False Reject probability (%)');


% -- CDET curve

% Settings
cost_fr        = 2;
cost_fa        = 1;
prob_genuine   = 0.5;
prob_imposture = 0.5;

% Compute costs
CDET = FR .* cost_fr .* prob_genuine + FA .* cost_fa .* prob_imposture;

% Find min threshold and its cost
[optimal_cost, min_idx] = min(CDET);
optimal_threshold = T(min_idx);

% Display
figure;
hold on;
plot(T, CDET, 'r'); 
plot(optimal_threshold, optimal_cost, 'bo');
title('CDET');
xlabel('Threshold');
ylabel('Cost');
