% Solve an Input-Output Fitting problem with a Neural Network

% -- Config
GENUINE_CLASS = 1; % ID of the genuine user (order of last name in alphabet)

HIDDEN_LAYER_SIZE = 10; % number of neurons in the hidden layer

TRAIN_RATIO       = 70/100; % percentage of samples in training set
TEST_RATIO        = 15/100; % percentage of samples in test set
VAL_RATIO         = 15/100; % percentage of samples in validation set

% Choose training function (see help nntrain)
% Default: 'trainlm' (Levenberg-Marquardt backpropagation)
TRAIN_FCN = 'trainlm';

% Choose performance function
% 'crossentropy': Cross-Entropy
% 'mse':          Mean Squared Error (default)
PERFORM_FCN = 'mse'; 

% -- Training

% Load input and output data
load('featuresAll.txt'); % input data (features) 
load('classesAll.txt');  % output data (classes), integers

% Transform data for network
outputFit = classesAll == 1; % output is in {0,1}
x = featuresAll';            % transpose to have samples as columns
t = outputFit';              % transpose to have ouput as colum

% Create a Fitting Network
net = fitnet(HIDDEN_LAYER_SIZE, TRAIN_FCN);

% Setup Division of Data for Training, Validation, Testing
net.divideParam.trainRatio = TRAIN_RATIO;
net.divideParam.valRatio = VAL_RATIO;
net.divideParam.testRatio = TEST_RATIO;

% Train the Network
[net,tr] = train(net,x,t);

% Test the Network
y = net(x);
e = gsubtract(t,y);
performance = perform(net,t,y);

% View the Network
%view(net)

% Plots
% Uncomment these lines to enable various plots.
figure, plotperform(tr)
figure, plotroc(t,y)
%figure, plottrainstate(tr)
%figure, plotfit(net,x,t)
%figure, plotregression(t,y)
%figure, ploterrhist(e)


