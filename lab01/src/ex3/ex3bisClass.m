% Solve a Pattern Recognition Problem with a Neural Network

% -- Config
GENUINE_CLASS = 1; % ID of the genuine user (order of last name in alphabet)

HIDDEN_LAYER_SIZE = 10; % number of neurons in the hidden layer

TRAIN_RATIO       = 70/100; % percentage of samples in training set
TEST_RATIO        = 15/100; % percentage of samples in test set
VAL_RATIO         = 15/100; % percentage of samples in validation set

% Choose training function (see help nntrain)
% Default: 'traiscg' (Scaled conjugate gradient backpropagation)
TRAIN_FCN = 'trainscg';

% Choose performance function
% 'crossentropy': Cross-Entropy (default)
% 'mse':          Mean Squared Error
PERFORM_FCN = 'mse';

% -- Training

% Load input and output data
load('featuresAll.txt'); % input data (features) 
load('classesAll.txt');  % output data (classes), integers

% Transform classes to use 2 outputs with values in {0,1}
class2All = [classesAll == 1, classesAll ~= 1];

% Tranpose to match neural network input format
x = featuresAll';
t = class2All';

% Create a Pattern Recognition Network
net = patternnet(HIDDEN_LAYER_SIZE);

% Setup Division of Data for Training, Validation, Testing
net.divideParam.trainRatio = TRAIN_RATIO;
net.divideParam.valRatio = VAL_RATIO;
net.divideParam.testRatio = TEST_RATIO;

% Setup performance function
net.performFcn = PERFORM_FCN;

% Train the Network
[net,tr] = train(net,x,t);

% Test the Network
y = net(x);
e = gsubtract(t,y);
tind = vec2ind(t);
yind = vec2ind(y);
percentErrors = sum(tind ~= yind)/numel(tind);
performance = perform(net,t,y);

% View the Network
%view(net)

% Plots
% Uncomment these lines to enable various plots.
figure, plotperform(tr)
figure, plotroc(t,y)
%figure, plotconfusion(t,y)
%figure, plottrainstate(tr)
%figure, ploterrhist(e)

