function [net,tr,scores,classVal] = trainNN(data,classId)

% Create a new NN for a specific ID class

net = feedforwardnet(10);
net.trainParam.showWindow = true;

% shuffle data
data = data(:,randperm(size(data,2)));

% x data : all line except the last (features)
% y data : classe of the features
class = data(end,:);
features = data(1:end-1,:);
class = class(end,:) == classId;

[net,tr] = train(net,features,class);

% validation of the result
featVal = features(:,tr.valInd);
classVal = class(:,tr.valInd);
scores = sim(net,featVal);

end

