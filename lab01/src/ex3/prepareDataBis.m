function [] = prepareDataBis(srcDir, featuresFile, classesFile)
%CREATEDATAFILE Summary of this function goes here
%   Detailed explanation goes here

% We need to load the txt files one by one in a specific directory
% and treat them

% setup files info
files = dir(srcDir);
classes = [];

% Create the data file to simplify coding
% and not use semi-dinamyc array of matlab (too slow)
featuresFileID = fopen(featuresFile,'w');
classesFileID = fopen(classesFile,'w');
currentClass = 0; % class of the current input file (last name)
lastName = ''; % just init
for file = files'
    % recover some data
    if file.isdir 
        % filter '.', '..', ...
        continue;
    end
    
    % Get the classe name
    classeName = strsplit(file.name,'-'); classeName = classeName(1);
    classes = [classes;classeName]; %#ok<AGROW>
    
    % determine classes
    if ~strcmp(classeName,lastName)
        currentClass = currentClass + 1;
    end
    lastName = classeName;
    
    % load file and save it (append) to the full data file
    name = [srcDir '/' file.name];
    tmpFile = load(name);
    for ii = 1:size(tmpFile,1)
        fprintf(featuresFileID,'%s\n',sprintf('%d,',tmpFile(ii,:)));
        fprintf(classesFileID,'%d\n', currentClass);
    end
end
fclose(featuresFileID);
end




