function [] = plotFn(scores,classVal)
%PLOTFN Summary of this function goes here
%   Detailed explanation goes here

genuine_scores = scores(classVal == 1);
impostures_scores = scores(classVal == 0);

min_score = min(scores);
max_score = max(scores);
step = 0.001;
T = min_score:step:max_score;
FR = zeros(1,size(T,2));
FA = zeros(1,size(T,2));

for tt = 1:size(T,2)
    Ttmp = T(tt);
    FR(tt) = sum(genuine_scores < Ttmp) ./ size(genuine_scores, 2);
    FA(tt) = sum(impostures_scores >= Ttmp) ./ size(impostures_scores, 2);
end

% -- T-FA/T-FR curves
figure;
plot(T,FA,'r'); hold on;
plot(T,FR,'r'); 

xlabel('Threshold');
ylabel('False Acceptance/Reject probability (%)');
title('T-FA, T-FR');

% -- ROC curve
figure;
plot(FA .* 100, FR .* 100, 'b');
xlabel('False Acceptance probability (%)');
ylabel('False Reject probability (%)');
title('ROC');

% -- DET curve
figure;

% DET curve
loglog(FA.*100,FR.*100,'g'); 
axis([0 100 0 100]);hold on;
% diagonal
x = 0:0.01:100;
plot(x,x,'r');

% Plot settings
title('DET');
axis([0 100 0 100]);
xlabel('False Acceptance probability (%)');
ylabel('False Reject probability (%)');

end

