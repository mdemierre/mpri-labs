function FeatureGeneratorTxt(file,path)
    addpath('feature-extraction'); 
    if strcmp(file,'.')
        return;
    end
    if strcmp(file,'..')
        return;
    end
    fprintf('%s\n',[path file]);
    [training_data,Fs,bits] = wavread([path file]);
    c = melcepst(training_data,Fs);

    csvwrite(['txtFiles/' file '.txt'], c);
end
