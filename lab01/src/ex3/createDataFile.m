function [] = createDataFile(srcDir,dataFile)
%CREATEDATAFILE Summary of this function goes here
%   Detailed explanation goes here

% We need to load the txt files one by one in a specific directory
% and treat them

if exist(dataFile) %#ok<EXIST>
    return;
end

% setup files info
files = dir(srcDir);
classes = [];
% Create the data file to simplify coding
% and not use semi-dinamyc array of matlab (too slow)
fileID = fopen(dataFile,'w');
numclasse = 0;
lastName = ''; % just init
for file = files'
    % recover some data
    if file.isdir 
        % filter '.', '..', ...
        continue;
    end
    
    % Get the classe name
    classeName = strsplit(file.name,'-'); classeName = classeName(1);
    classes = [classes;classeName]; %#ok<AGROW>
    
    % determine classes
    if ~strcmp(classeName,lastName)
        numclasse = numclasse + 1;
    end
    lastName = classeName;
    
    % load file and save it (append) to the full data file
    name = [srcDir '/' file.name];
    tmpFile = load(name);
    for ii = 1:size(tmpFile,1)
        fprintf(fileID,'%s %d\n',sprintf('%d,',tmpFile(ii,:)),numclasse);
    end
end
fclose(fileID);
end




