clear;
close all;

% Exercise 3 : ANN to for a voice-based access system

% -- : Load data
srcDir = 'txtFiles';
dataFile = 'dataAll.txt';

fprintf('%s\n','create data file');
createDataFile(srcDir,dataFile);
fprintf('%s\n','load data file');
data = load(dataFile);
data = data'; % need to transpose for network use

% class define for our group
% 1 is by default, need to change something to determine classes
% for each personn in the MPRI curse
[net,tr,scores,classValidationInd] = trainNN(data,4);

plotFn(scores,classValidationInd);