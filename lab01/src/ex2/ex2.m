% Exercice 2 : ANN to estimate a cos(x) function
fn = @(x) cos(x);

% Part (a)
% Generate a sample of N random data between [-20;20]
% and save him into a file
N     = 1000;
dataX = rand(1,N/2).*20;
dataX = [dataX,rand(1,N/2).*-20];
dataX = dataX(randperm(N));
dataY = feval(fn,dataX);
data  = [dataX;dataY];
save('data.mat','data');

% net.layers{1}.transferFcn : tansig
net = feedforwardnet(100);

net.trainParam.showWindow = false;
% net.trainParam.epochs = 20;

[net,tr] = train(net,dataX,dataY);

figure;
plotfit(net,dataX,dataY);

figure;
plotperform(tr);
% mse(net,dataX,dataY);
